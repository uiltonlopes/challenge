<?php
  session_start();
 ?>
 <!DOCTYPE html>
 <html lang="pt-br">
 <head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
   <title>Engine Smart Solutions - Download</title>

   <!-- CSS  -->
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <link href="assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
   <link href="assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
 </head>
 <body>
   <nav class="teal" role="navigation">
     <div class="nav-wrapper container"><a id="logo-container" href="/" class="brand-logo">ESS</a>
       <ul class="right hide-on-med-and-down">
         <li><a href="/">Home</a></li>
       </ul>
       <ul id="nav-mobile" class="side-nav">
         <li><a href="/">Home</a></li>
       </ul>
       <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
     </div>
   </nav>
   <div class="section no-pad-bot">
     <div class="container">
       <h1 class="header center">Obrigado!</h1>
     </div>
   </div>


   <div class="container">
     <div class="section">

       <!--   Icon Section   -->
       <div class="row">
         <div class="col s12 m6">
           <h5>Foi enviado um email para o endereço <b><?php echo $_SESSION['email']; ?></b> contendo o arquivo convertido em anexo.</h5>
         </div>
         <div class="col s12 m6">
           <a class="btn waves-effect waves-light" href="<?php echo $_SESSION['file']; ?>">Download</a>
          </form>
         </div>
       </div>
     </div>
   </div>



   <!--  Scripts-->
   <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
   <script src="assets/js/materialize.js"></script>
   <script src="assets/js/init.js"></script>

   </body>
 </html>
