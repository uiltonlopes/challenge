<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Engine Smart Solutions</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="teal" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="/" class="brand-logo">ESS</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="/">Home</a></li>
      </ul>
      <ul id="nav-mobile" class="side-nav">
        <li><a href="/">Home</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <div class="section no-pad-bot">
    <div class="container">
      <h1 class="header center">Excel para CSV</h1>
    </div>
  </div>


  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m6">
          <h5>Os arquivos convertidos serão enviados para o email em forma de anexo.</h5>
        </div>
        <div class="col s12 m6">
          <form enctype="multipart/form-data" action="convert.php" method="post">
            <div class="row">
              <div class="input-field col s12">
                <input type="email" class="validate" name="email" required>
                <label for="email">Email</label>
              </div>
            </div>
            <div class="row">
              <div class="file-field input-field col s12">
                <div class="btn">
                  <span>Arquivo</span>
                  <input type="file" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text">
                </div>
              </div>
            </div>
           <div class="row">
             <button class="btn waves-effect waves-light" type="submit" name="action">Converter</button>
           </div>
         </form>
        </div>
      </div>
    </div>
  </div>

  <footer class="page-footer teal">
    <div class="container">
      <div class="row">
        <div class="col l12 s12">
          <h5 class="white-text center">Engine Smart Solutions</h5>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="assets/js/materialize.js"></script>
  <script src="assets/js/init.js"></script>

  </body>
</html>
