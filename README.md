# Desafio Being Marketing

Para a construção do projeto foi utilizado PHP para o back-end, banco de dados MySQL, o framework Materialize CSS para o front-end, as libs PHPExcel para conversão em arquivos CSV e  PHPmailer para envio de emails.

Hospedei o projeto em um servidor free para melhor análise e teste.

## LINK

* [Desafio](https://uilton.000webhostapp.com/) - Link do desafio
