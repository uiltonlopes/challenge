<?php

  require_once 'conect.php';
  require_once 'lib/Classes/PHPExcel/IOFactory.php';
  require_once('lib/email/class.phpmailer.php');


  $path = 'uploads/';


  if(is_dir($path))
  {
    if($handle = opendir($path))
    {
      while(($file = readdir($handle)) !== false)
      {
        if($file != '.' && $file != '..')
        {
          unlink($path.$file);
        }
      }
    }
  }



  session_start();
  $emailUser = $_POST['email'];
  $_SESSION['email'] = $emailUser;
  $fileTmp = $_FILES['file']['tmp_name'];
  $fileName = $_FILES['file']['name'];
  $fileSave = preg_replace('/\.[^.]*$/', '', basename($fileName));


  $inputFileType = 'Excel2007';
  $inputFileName = $fileTmp;

  $objReader = PHPExcel_IOFactory::createReader($inputFileType);
  $objPHPExcelReader = $objReader->load($inputFileName);

  $loadedSheetNames = $objPHPExcelReader->getSheetNames();

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcelReader, 'CSV');

  foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
    $objWriter->setSheetIndex($sheetIndex);
    $objWriter->save('uploads/'.$fileSave.'.csv');
  }

  $ficheiro = $fileSave.'.csv';

  //Definiçoes de email configuradas para funcionar no servidor
  $mail = new PHPMailer(true);
  $mail->SetFrom('convert@ess.com', 'Engine Smart Solutions');
  $mail->AddAddress($emailUser, 'Destinatário');
  $mail->Subject = 'Arquivo convertido';
  $mail->AltBody = 'Segue em anexo o arquivo convertido para o formato CSV.';
  $mail->MsgHTML('Segue em anexo o arquivo convertido para o formato CSV.</p>');
  $mail->AddAttachment($path.$ficheiro);
  $mail->Send();



  $fildownload = 'uploads/'.$fileSave.'.csv';
  $_SESSION['file'] = $fildownload;

  $saveLog = "INSERT INTO challenge_log(email,name,register) VALUES ('$emailUser','$ficheiro',NOW())";

  $result_saveLog = mysqli_query($conn,$saveLog);

  $redirect = "download.php";
  header("location:$redirect");


 ?>
